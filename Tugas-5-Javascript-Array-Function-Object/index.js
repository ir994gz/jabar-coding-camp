//Soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for (var i = 0; i < daftarHewan.length; i++) {
  console.log(daftarHewan.find((a) => a.includes((i + 1).toString())));
}

//Soal 2

function introduce(params) {
  return (
    "Nama saya " +
    params.name +
    ", umur saya " +
    params.age +
    " tahun, alamat saya di " +
    params.address +
    ", dan saya punya hobby yaitu " +
    params.hobby
  );
}
var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// Soal 3

function hitung_huruf_vokal(params) {
  var vocal = 0;

  for (var i = 0; i < params.length; i++) {
    if (
      params[i].toLowerCase() == "a" ||
      params[i].toLowerCase() == "i" ||
      params[i].toLowerCase() == "u" ||
      params[i].toLowerCase() == "e" ||
      params[i].toLowerCase() == "o"
    )
      vocal++;
  }

  return vocal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Saya Iqbal");

console.log(hitung_1, hitung_2);

//Soal 4

function hitung(params) {
  return params * 2 - 2;
}
console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
