var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

readBooksPromise(10000, books[0]).then((value) =>
  readBooksPromise(value, books[1]).then((value2) =>
    readBooksPromise(value2, books[2]).then((value3) =>
      readBooksPromise(value3, books[3])
    )
  )
);
