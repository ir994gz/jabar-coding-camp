//Soal 1

var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

readBooks(10000, books[0], function (timeSpent) {
  readBooks(timeSpent, books[1], function (timeSpent1) {
    readBooks(timeSpent1, books[2], function (timeSpent2) {
      readBooks(timeSpent2, books[3], function (timeSpent3) {
        console.log(timeSpent3);
      });
    });
  });
});
