//Soal No 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var pertamaSplit = pertama.split(" ")
var keduaSplit = kedua.split(" ")

var hasil = pertamaSplit[0]+" "+pertamaSplit[2]+" "+keduaSplit[0]+" "+keduaSplit[1].toUpperCase()
console.log(hasil)

//Soal 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var output = (parseInt(kataPertama)+parseInt(kataKeempat)-parseInt(kataKetiga))*parseInt(kataKedua)
console.log(output)


//Soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14)
var kataKetiga = kalimat.substring(15,18)
var kataKeempat=  kalimat.substring(19,24)
var kataKelima = kalimat.substring(25,31)

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

