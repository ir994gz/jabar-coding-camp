//Soal 1

const luas = (panjang, lebar) => {
  return panjang * lebar;
};

const keliling = (panjang, lebar) => {
  return 2 * (panjang + lebar);
};

//Soal 2
const newFunction = (literals = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName() {
      console.log(firstName + " " + lastName);
    },
  };
});

newFunction("William", "Imoh").fullName();

//Soal 3

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

//Soal 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

// Soal 5

const planet = "earth";
const view = "glass";
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(before);
