export const MemberComponent = {
  template: `
        <table>
          <tr v-for="(member,index) in members">
            <td>
              <img
                :src='member.photo_profile ? api+member.photo_profile : "https://dummyimage.com/250/ffffff/000000"'
                alt=""
              />
            </td>
            <td>
              <p><b>Name :</b> {{member.name}}</p>
              <p><b>Address :</b> {{member.address}}</p>
              <p><b>Phone :</b> {{member.no_hp}}</p>
            </td>
            <td>
                          <button @click="$emit('ch-edit',index)">Edit</button>

              <button @click="$emit('ch-delete',member.id)">Delete</button>
              <button @click="$emit('ch-upload',index)">Upload Photo</button>
            </td>
          </tr>
        </table>

    `,
  data() {
    return {};
  },

  props: ["members", "api"],
};
