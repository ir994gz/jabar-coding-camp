//Soal 1

var nilai = 75;

if (nilai >= 85) console.log("A");
if (nilai >= 75 && nilai < 85) console.log("B");
if (nilai >= 65 && nilai < 75) console.log("C");
if (nilai >= 55 && nilai < 65) console.log("D");
if (nilai < 55) console.log("E");

//Soal 2

var tanggal = 21;
var bulan = 2;
var tahun = 1994;
var output;

switch (bulan) {
  case 1:
    output = (tanggal + " " + "Januari " + tahun).toString();
    console.log(output);

    break;
  case 2:
    output = (tanggal + " " + "Februari " + tahun).toString();
    console.log(output);

    break;
  case 3:
    output = (tanggal + " " + "Maret " + tahun).toString();
    console.log(output);

    break;
  case 4:
    output = (tanggal + " " + "April " + tahun).toString();
    console.log(output);

    break;
  case 5:
    output = (tanggal + " " + "Mei " + tahun).toString();
    console.log(output);

    break;
  case 6:
    output = (tanggal + " " + "Juni " + tahun).toString();
    console.log(output);

    break;
  case 7:
    output = (tanggal + " " + "Juli " + tahun).toString();
    console.log(output);

    break;
  case 8:
    output = (tanggal + " " + "Agustus " + tahun).toString();
    console.log(output);

    break;
  case 9:
    output = (tanggal + " " + "September " + tahun).toString();
    console.log(output);

    break;
  case 10:
    output = (tanggal + " " + "Oktober " + tahun).toString();
    console.log(output);

    break;
  case 11:
    output = (tanggal + " " + "November " + tahun).toString();
    console.log(output);

    break;
  case 12:
    output = (tanggal + " " + "Desember " + tahun).toString();
    console.log(output);

    break;

  default:
    break;
}

//Soal 3
var len = 3;
var crest = "";

for (var i = 0; i < len; i++) {
  crest += "#";
  console.log(crest);
}

//Soal 4
var m;
var cmpr = "";
m = 10;
j = 0;

for (var i = 1; i <= m; i++) {
  switch (i - j * 3) {
    case 1:
      console.log(i + " - I love programming");
      break;
    case 2:
      console.log(i + " - I love Javascript");
      break;
    case 3:
      console.log(i + " - I love VueJS");
      j++;

      for (var k = 0; k < j * 3; k++) {
        cmpr += "=";
      }
      console.log(cmpr);
      cmpr = "";
      break;

    default:
      break;
  }
}
