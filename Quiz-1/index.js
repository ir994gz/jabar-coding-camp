function jumlah_kata(params) {
  var splitParams = params.split(" ");
  for (let index = 0; index < splitParams.length; index++) {
    const element = splitParams[index];
    if (element == "") splitParams.splice(index, 1);
  }
  return splitParams.length;
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));

function next_date(tanggal, bulan, tahun) {
  switch (bulan) {
    case 1:
      if (tanggal >= 31) {
        bulan = "Februari";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Januari " + tahun.toString();
      }

    case 2:
      if (tanggal >= 28) {
        bulan = "Maret";
        return "1 " + bulan + " " + tahun.toString();
      } else if (tanggal >= 29 && tahun % 4 == 0) {
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Februari " + tahun.toString();
      }

    case 3:
      if (tanggal >= 31) {
        bulan = "April";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Maret " + tahun.toString();
      }

    case 4:
      if (tanggal >= 30) {
        bulan = "Mei";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " April " + tahun.toString();
      }

    case 5:
      if (tanggal >= 31) {
        bulan = "Juni";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Mei " + tahun.toString();
      }

    case 6:
      if (tanggal >= 30) {
        bulan = "Juli";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Juni " + tahun.toString();
      }

    case 7:
      if (tanggal >= 31) {
        bulan = "Agustus";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Juli " + tahun.toString();
      }

    case 8:
      if (tanggal >= 31) {
        bulan = "September";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Agustus " + tahun.toString();
      }

    case 9:
      if (tanggal >= 30) {
        bulan = "Oktober";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " September " + tahun.toString();
      }

    case 10:
      if (tanggal >= 31) {
        bulan = "November";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Oktober " + tahun.toString();
      }

    case 11:
      if (tanggal >= 30) {
        bulan = "Desember";
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " November " + tahun.toString();
      }

    case 12:
      if (tanggal >= 31) {
        bulan = "Januari";
        tahun++;
        return "1 " + bulan + " " + tahun.toString();
      } else {
        tanggal++;
        return tanggal.toString() + " Desember " + tahun.toString();
      }

    default:
      break;
  }
}

console.log(next_date(31, 1, 2020));

var tanggal = 28;
var bulan = 2;
var tahun = 2021;

console.log(
  next_date(tanggal, bulan, tahun) // output : 1 Maret 2021
);

var tanggal = 31;
var bulan = 12;
var tahun = 2020;
console.log(next_date(tanggal, bulan, tahun));
// output : 1 Januari 2021
